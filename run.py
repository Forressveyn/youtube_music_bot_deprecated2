from __future__ import unicode_literals
import os
import logging
import telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
import lyricsgenius as genius
import youtube_dl

token = '1244228269:AAGHO5Z5pYJ0C1z8aNdjpsKPnkwI7jmNg6c'

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG
)
logger = logging.getLogger(__name__)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': logger,
    'progress_hooks': [my_hook],
    'nocheckcertificate': True,
}


cred = 'OG0n_1GvO2aAbJM9QK5jKPOO8vg7UuXSiZB6pykLYsDLFkkeT3y_YP6z0p92XSmr'
api = genius.Genius(cred)


def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="I'm a bot, please talk to me!")


def search(update, context):

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Please, wait for a minute. \nI'm working",
        parse_mode=telegram.ParseMode.MARKDOWN
    )

    search_string = ' '.join(context.args)

    result = api.search_genius(search_string)

    songs = []

    for value in result['hits']:

        if value['type'] == 'song':
            song = api.get_song(value['result']['id'])['song']

            for item in song['media']:
                if item['provider'] == 'youtube':
                    songs.append(song)

    keyboard = [
        [InlineKeyboardButton(
            f'{song["primary_artist"]["name"]} | {song["title"]}',
            callback_data=str(song['id'])
        )] for song in songs
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Please choose:', reply_markup=reply_markup)


def button(update, context):
    query = update.callback_query

    song = api.get_song(query.data)['song']

    filename = None

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:

        for item in song['media']:
            if item['provider'] == 'youtube':
                data = ydl.extract_info(item['url'])
                data['ext'] = 'mp3'
                filename = ydl.prepare_filename(data)

    answer = ''

    if song.get('primary_artist') and 'name' in song.get('primary_artist'):
        answer = f'{answer}<b>Artist</b>: {song["primary_artist"]["name"]}\n'

    if song.get('title'):
        answer = f'{answer}<b>Title</b>: {song["title"]}\n'

    if song.get('album') and 'name' in song.get('album'):
        answer = f'{answer}<b>Album</b>: {song["album"]["name"]}\n'

    if song.get('recording_location'):
        answer = f'{answer}<b>Recording location</b>: ' \
                 f'{song["recording_location"]}\n'

    if song.get('release_date_for_display'):
        answer = f'{answer}<b>Release date</b>: ' \
                 f'{song["release_date_for_display"]}\n'

    if song.get('description') and 'plain' in song.get('description'):
        answer = f'{answer}<b>Description</b>:\n' \
                 f'{song["description"]["plain"]}\n'

    query.answer()

    query.edit_message_text(text=answer, parse_mode=telegram.ParseMode.HTML)

    context.bot.send_audio(
        query.message.chat_id,
        audio=open(filename, 'rb')
    )

    os.remove(filename)


def help(update, context):
    update.message.reply_text("Use /start to test this bot.")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('search', search))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    main()